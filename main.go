package main

import (
	"fmt"
	"log"

	"net/http"
	"encoding/json"
	"io/ioutil"

	"github.com/julienschmidt/httprouter"
)

type person struct {
	Name string `json:"name"`
	BirthYear int `json:"birth_year"`
}

type answer struct {
	Msg string `json:"message"`
}

func SamplePOST(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	var p person 
	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalf("%s: %s","Failed to read req body",err)
	}

	json.Unmarshal(reqBody, &p) //You should check the err returned by json.Unmarshall

	msg := fmt.Sprintf( "Hello %s! You should be %v years old.", p.Name, 2020 - p.BirthYear ) 
	a := answer { Msg: msg } 

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(a)
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	fmt.Fprint(w,"Welcome!\n")
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params){
	fmt.Fprintf(w,"hello, %s!\n", ps.ByName("person"))
}

func main(){
	router := httprouter.New()
	router.GET("/", Index)
	router.GET("/hello/:person", Hello)
	router.POST("/age", SamplePOST)
	log.Fatal(http.ListenAndServe(":5500", router))
}
