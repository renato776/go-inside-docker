FROM golang:latest

WORKDIR /go/src/gitlab.com/Renato776/go-inside-docker

COPY . .

RUN go build -o goServer

ENTRYPOINT ./goServer

EXPOSE 5500
